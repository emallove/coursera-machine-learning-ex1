function J = computeCost(X, y, theta)

  %COMPUTECOST Compute cost for linear regression
  %   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
  %   parameter for linear regression to fit the data points in X and y

  % Initialize some useful values
  m = length(y); % number of training examples

  % You need to return the following variables correctly 

  % We aim for a perfect fit - if the cost is 0 - we have fit the 
  % training set with 0 error
  J = 0;

  % ============= START:   YOUR CODE HERE ======================
  % Instructions: Compute the cost of a particular choice of theta
  %               You should set J to the cost.

  % type("theta")
  % theta

  % type("y")
  % y
  % length(y)
  % y(1)
  % y(2)
  % y(3)
  % y(4)
  % y(5)

  m = length(X)

  theta1 = theta(1)
  theta2 = theta(2)

  disp(theta1);
  disp(theta2);
  disp(m);

  % partial return value, because who knows what happens if we 
  % mutate with the return value more than once
  % _J = 0

  % Index into y vector.  Note, we're 1-indexed, not 0-indexed.  Welcome to Octave.
  _i = 1

  % TODO: instead of an iterator, use some fancy-schmancy 
  % linear algebra functions to utilize parallelism


  %  This for loop is wrong.
  %  _x is not getting set to a scalar!  E.g.,
  %
  %  Attr Name            Size                     Bytes  Class
  % ==== ====            ====                     =====  ===== 
  %      X              97x1                        776  double
  %      aa              1x3                         24  double
  for _x = X

    %
    % _x ** y --> _x to the power y
    %

    %
    % Use .**, instead of plain ** (no dot) to avoid this error
    %   for A^b, A must be square
    %

    disp(_x)
    disp(y(_i))

    J += (1/(2*m)) * (((theta1 + (theta2 * _x)) - (y(_i++))) .** 2)

  endfor

  % this is how we return J in Octave
  % J = _J

  disp(J)

  % ================ END:  YOUR CODE HERE ======================

end
